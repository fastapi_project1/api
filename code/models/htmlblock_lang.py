from sqlalchemy import Column, Integer, Text, String, ForeignKey
from sqlalchemy.orm import relationship
from . import Base


class HtmlblockLang(Base):
    __tablename__ = 'htmlblock_lang'

    id = Column(Integer, primary_key=True, index=True)
    textblock = Column(Text, nullable=True)
    lang_id = Column(Integer, ForeignKey('lang.id', ondelete='CASCADE'), nullable=False)
    parent_id = Column(Integer, ForeignKey('htmlblock.id', ondelete='CASCADE'), nullable=False)

    htmlblock_lang = relationship('Lang', back_populates='htmlblock_lang')
    htmlblock = relationship('Htmlblock', back_populates='htmlblock')