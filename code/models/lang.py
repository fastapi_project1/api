from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship
from . import Base  # Импортируем Base из __init__.py
from .htmlblock_lang import HtmlblockLang

class Lang(Base):
    __tablename__ = 'lang'

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, nullable=False)

    htmlblock_lang = relationship('HtmlblockLang', back_populates='htmlblock_lang')
    category_lang = relationship('CategoryLang', back_populates='category_lang')