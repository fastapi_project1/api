from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship
from . import Base

class Category(Base):
    __tablename__ = 'category'

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, nullable=False)
    parent_category = Column(Integer, nullable=False, index=True)

    category = relationship('CategoryLang', back_populates='category')