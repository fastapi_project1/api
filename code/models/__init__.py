from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

from .user import User

from .lang import Lang
from .htmlblock import Htmlblock
from .htmlblock_lang import HtmlblockLang
from .category import Category
from .category_lang import CategoryLang


# Здесь мы устанавливаем базовый класс для всех моделей
User.__table__.metadata = Base.metadata
Lang.__table__.metadata = Base.metadata
Htmlblock.__table__.metadata = Base.metadata
HtmlblockLang.__table__.metadata = Base.metadata
Category.__table__.metadata = Base.metadata
CategoryLang.__table__.metadata = Base.metadata