from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship
from . import Base

class Htmlblock(Base):
    __tablename__ = 'htmlblock'

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, nullable=False)


    htmlblock = relationship('HtmlblockLang', back_populates='htmlblock')