from fastapi import HTTPException
from models.user import User

# Класс для работы методов авторизированных пользователей
class ApiAuthView:
    #Конструктор класса
    def __init__(self, db):
        self.db = db

    #Выводим данные пользователя
    def getUserDataByToken(self,phone):
        db_session = self.db
        DbUSer = self.db.query(User).filter(User.phone == phone).first()
        self.db.commit()
        if DbUSer is None:
            raise HTTPException(status_code=404, detail="Not found record")
        try:
            arrayDataUser={"name": DbUSer.name,
                           "phone": DbUSer.phone,
                           "is_active":DbUSer.is_active,
                           "is_admin": DbUSer.is_admin}
        except Exception as e:
            db_session.rollback()
            raise HTTPException(status_code=500, detail="Failed  login")
        finally:
            db_session.close()
        return arrayDataUser