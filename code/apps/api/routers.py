from fastapi import APIRouter, Depends, Form, HTTPException
from fastapi.security import OAuth2PasswordBearer
from apps.api.view_auth import ApiAuthView
from database import SessionLocal
from passlib.context import CryptContext
from jose import jwt,JWTError
from dotenv import load_dotenv
from pathlib import Path
from datetime import datetime, timedelta
import os

env_path =  '/gtapp/.env'
load_dotenv(dotenv_path=env_path)
# Configure JWT settings
SECRET_KEY = os.getenv('SECRET_KEY')
ALGORITHM = os.getenv('ALGORITHM')
ACCESS_TOKEN_EXPIRE_MINUTES = 15
REFRESH_TOKEN_EXPIRE_DAYS = 7


router = APIRouter()

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="login")
async def get_user_from_token(token: str = Depends(oauth2_scheme)):
    try:
       payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
       return payload.get("sub")
    except jwt.ExpiredSignatureError:
       print("тут какая-то логика ошибки истечения срока действия токена")
       pass  # тут какая-то логика ошибки истечения срока действия токена
    except jwt.InvalidTokenError:
       raise HTTPException(status_code=401, detail="Invalid token")

@router.post("/userdata")
def post_datauser(current_user: str = Depends(get_user_from_token)):
    #Получаем сессию  БД
    session = SessionLocal()
    #конструктор представления
    auth_view = ApiAuthView(session)
    #возврошаем данные
    return {'data':auth_view.getUserDataByToken(current_user)}
