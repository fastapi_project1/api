from fastapi import HTTPException
from models.user import User
from passlib.context import CryptContext
from jose import jwt
from dotenv import load_dotenv
from pathlib import Path
from datetime import datetime, timedelta
import os

env_path =  '/gtapp/.env'
load_dotenv(dotenv_path=env_path)
# Configure JWT settings
SECRET_KEY = os.getenv('SECRET_KEY')
ALGORITHM = os.getenv('ALGORITHM')
ACCESS_TOKEN_EXPIRE_MINUTES = 15
REFRESH_TOKEN_EXPIRE_DAYS = 7

#генерируем токен
def create_jwt_token(data: dict, expires_delta: timedelta):
    to_encode = data.copy()
    expire = datetime.utcnow() + expires_delta
    to_encode.update({"exp": expire})
    return jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)

def create_access_token(data: dict):
    return create_jwt_token(data, timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES))

def create_refresh_token(data: dict):
    return create_jwt_token(data, timedelta(days=REFRESH_TOKEN_EXPIRE_DAYS))

# Хешируем пароль
pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
def hash_pass(password:str):
    return pwd_context.hash(password)


class AuthView:
    #Конструктор класса
    def __init__(self, db):
        self.db = db

    #авторизация
    def loginUser(self,phone: str,password: str):
        db_session = self.db
        DbUSer = self.db.query(User).filter(User.phone == phone).first()
        self.db.commit()
        if DbUSer is None:
            raise HTTPException(status_code=404, detail="Not found record")
        is_verified = pwd_context.verify(password, DbUSer.hashed_password)
        try:
            if not is_verified:
                raise HTTPException(status_code=500, detail="Failed  login")

            access_token = create_access_token({"sub": phone})
            refresh_token = create_refresh_token({"sub": phone})
            db_session.close()
            return {"access_token": access_token, "refresh_token": refresh_token}
        except Exception as e:
            print(e)
            db_session.rollback()
            raise HTTPException(status_code=500, detail="Failed  login")
        finally:
            db_session.close()
        raise HTTPException(status_code=500, detail="Failed  login")

    # Register new  user
    def setSignUp(self,username: str,password: str, phone: str):
        db_session = self.db
        hashUserPassowrd = hash_pass(password)
        try:
            NewUser = User(phone=phone,
                           name=username,
                           hashed_password=hashUserPassowrd,
                           is_admin=0,
                           is_active=1)
            db_session.add(NewUser)
            db_session.commit()
        except Exception as e:
            db_session.rollback()
            raise HTTPException(status_code=500, detail=f"Failed to retrieve user data: {str(e)}")
        finally:
            db_session.close()
        return 1
