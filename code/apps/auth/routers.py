from fastapi import APIRouter, Depends, Form
from apps.auth.views import AuthView
from database import SessionLocal
from schemas.auth import LoginData, SingupData

router = APIRouter()

@router.post("/login")
def post_auth_login(data:LoginData):
    #Получаем сессию  БД
    session = SessionLocal()
    #конструктор представления
    auth_view = AuthView(session)
    #получаем данные которые были отправлены
    phone = data.phone
    password = data.password
    #вызов метода
    tokens=auth_view.loginUser(phone,password)
    #возврошаем метод
    return tokens

#метод регистрации пользователя
@router.post("/signup")
def post_auth_signup(data:SingupData):
    # Получаем сессию  БД
    session = SessionLocal()
    # получаем данные которые были отправлены
    username = data.username
    password = data.password
    phone = data.phone
    # конструктор представления
    auth_view = AuthView(session)
    auth_view.setSignUp(username, password, phone)
    return {"status": "ok","detail":"User create"}
