from pydantic import BaseModel

class LoginData(BaseModel):
    phone: str
    password: str

class SingupData(BaseModel):
    phone: str
    username: str
    password: str