import sqlalchemy
from databases import Database
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, Session
from dotenv import load_dotenv
#from pathlib import Path
import os

# Загрузка переменных окружения из файла .env
env_path =  '/gtapp/.env'
load_dotenv(dotenv_path=env_path)

DATABASE_URL = os.getenv('DATABASEURL')

database = Database(DATABASE_URL)
metadata = sqlalchemy.MetaData()
engine = create_engine(DATABASE_URL)
connection = engine.connect()
SessionLocal = sessionmaker(bind=engine)