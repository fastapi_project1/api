FROM python:3.12

#
WORKDIR /gtapp

#
RUN python3 -m pip config --user set global.index-url https://pypi.tuna.tsinghua.edu.cn/simple
RUN python3 -m pip config --user set global.timeout 150
RUN pip install --upgrade pip

#
COPY ./requirements.txt /gtapp/requirements.txt

#
RUN pip install --no-cache-dir --upgrade -r /gtapp/requirements.txt

#
COPY ./code /gtapp

#
CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "80","--reload"]

